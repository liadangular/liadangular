import { Component, OnInit } from '@angular/core';
import{PostsService} from './posts.service';


@Component({
  selector: 'jce-posts',

  templateUrl: './posts.component.html',
 
  styles: [`
    .posts li { cursor: default; } 
    .posts li:hover { background: #ecf0f1; }
    .list-group-item.active, .list-group-item.active:hover { 
         border-color: #ecf0f1; 
         color: #2c3e50;
  `]
})

export class PostsComponent implements OnInit {
//הגדרת משתנים
posts;
currentPost;  

//יצירת פונקציה שבוחרת users מתוך הנתונים  
  select(post)
  {
    this.currentPost = post;

  }

  constructor(private _postsService:PostsService) { }

  ngOnInit() {
        this.posts = this._postsService.getPosts();

  }
  


}