import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Post } from './post';
@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
    inputs:['post']

})
export class PostComponent implements OnInit {
 post:Post;
  @Output() deleteEvent = new EventEmitter<Post>();
  isEdit:Boolean = false;
  editButtonText = 'Edit';
  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.post);
  }
  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit? this.editButtonText = 'Save' : this.editButtonText = 'Edit'
  }

  ngOnInit() {
  }

}


