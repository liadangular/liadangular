import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from '../post/post';
import {NgForm} from '@angular/forms';
@Component({
  selector: 'jce-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {

@Output() postAddedEvent = new EventEmitter<Post>();
  post:Post = {title: '',content:'',author:''};
  onSubmit(form:NgForm) {
    console.log(form)
    this.postAddedEvent.emit(this.post);
    this.post = {
      title:'',
      content:'',
        author:''
    }
  }
  constructor() { }

  ngOnInit() {
  }

}

